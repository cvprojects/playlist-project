### Prerequisites

- .Net 4.7.2
- Visual Studio 2019 ( for development only )

### How to run

Double click "Playlist.exe" if you want to run the application, or open "Proiect.sln" if you want to edit the source code.

![Picture](Playlist.JPG)
