Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.ReadOnly = True
        TextBox2.ReadOnly = True
        If (RadioButton1.Checked) Then
            TextBox1.Visible = True
            TextBox1.Clear()
            TextBox1.Text = TextBox1.Text + " Nicu Alifantis"
            GroupBox2.Visible = True
            Label3.Visible = True
        End If

        If (RadioButton2.Checked) Then
            TextBox1.Visible = True
            TextBox1.Clear()
            TextBox1.Text = TextBox1.Text + " Iris"
            GroupBox2.Visible = False
            GroupBox3.Visible = True
            Label3.Visible = True

        End If

        If (RadioButton3.Checked) Then
            TextBox1.Visible = True
            TextBox1.Clear()
            TextBox1.Text = TextBox1.Text + " Michael Jackson"
            GroupBox2.Visible = False
            GroupBox3.Visible = False
            GroupBox4.Visible = True
            Label3.Visible = True
        End If

        If (RadioButton3.Checked) Then
            GroupBox2.Visible = False
            GroupBox3.Visible = False
            Label3.Visible = True

        End If

        If (RadioButton2.Checked) Then
            GroupBox2.Visible = False
            GroupBox4.Visible = False
            Label3.Visible = True
            RadioButton12.Checked = False
            RadioButton11.Checked = False
            RadioButton4.Checked = False
        End If

        If (RadioButton1.Checked) Then
            GroupBox3.Visible = False
            GroupBox4.Visible = False
            Label3.Visible = True
            RadioButton12.Checked = False
            RadioButton11.Checked = False
            RadioButton4.Checked = False
            RadioButton8.Checked = False
            RadioButton9.Checked = False
            RadioButton10.Checked = False
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If (RadioButton5.Checked) Then
            My.Computer.Audio.Play(My.Resources.Nicu_Alifantis___Ploaie_in_luna_lui_Marte, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Text =
        "Ploua infernal,
si noi ne iubeam prim mansarde.
Prin cerul ferestrei, prin cerul oval,
norii curgeau, in luna lui Marte.

Peretii odaii erau
nelinistiti, sub desene de creta.
Sufletele noastre dansau
nevazute-ntr-o lume concreta.
"       
        End If
        If (RadioButton6.Checked) Then
            My.Computer.Audio.Play(My.Resources.Nicu_Alifantis__Ce_bine_ca_eşti, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "Du-ma fericire-n sus,
Izbeste-mi tampla de stele,
Lumea prealunga si in nesfarsire,
Se face coloana sau altceva
Mult mai inalt si mult mai curand. (x2)

Ce bine ca esti, ce mirare ca sunt!
Doua cantece diferite, lovindu-se, amestecandu-se,
Doua culori, ce nu s-au vazut niciodata,
Una foarte jos, intoarsa spre pamant,
Una foarte sus, aproape rupta
In infrigurata, neasemuita lupta
A minunii ca esti, a-ntamplarii ca sunt."
        
        End If
        If (RadioButton7.Checked) Then
            My.Computer.Audio.Play(My.Resources.Nicu_Alifantis___Emotie_de_toamna, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "A venit, a venit toamna, 
Acopera-mi inima cu ceva,
Cu umbra unui copac sau mai bine
Sau mai bine cu umbra ta...

Ma tem ca n-am sa te mai vad, uneori,
Ca au sa-mi creasca aripi ascutite pan' la nori,
Ca ai sa te ascunzi intr-un ochi strain,
Si el o sa se-nchida cu o frunza de pelin.
"

        End If
        If (RadioButton8.Checked) Then
            My.Computer.Audio.Play(My.Resources.iris_Floare_de_iris, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "Floare de iris,
Floare de colt,
Ne-ai ocrotit,
Ne-ai dat un rost.

Iar cand noi nu vom mai fi,
Va veti aminti
Ca a fost odata Iris."

        End If
        If (RadioButton9.Checked) Then
            My.Computer.Audio.Play(My.Resources.Iris___Baby__cu_versuri_, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "Cat am fost de singur tu nu ai stiut,
Ti-ai ferit mereu privirea de la inceput,
Anii trec si viata merge iar,
Te caut zi si noapte, te caut, dar stiu ca e-n zadar!


Baby stiu acum ca mergi sa intalnesti
Un strain ce te va duce la casa din povesti,
Ce noroc, ce mare nenoroc
Ca timpul stinge totul,
Dar focul naste foc!"
       
        End If
        If (RadioButton10.Checked) Then
            My.Computer.Audio.Play(My.Resources.Iris___Vino_pentru_totdeauna, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "Spune-mi dacă, am greşit ceva,
Spune-mi dacă te-am pierdut cândva,
Nu mai ştiu cum să te caut,
Să mă-ntorc din nou la locul unde
Ne-am găsit, iubita mea!

Spune-mi oare ce mai ştii de noi?
Spune-mi oare suntem amandoi?
Doar o ploaie-n miez de vară care vine şi s-a dus,
Sub un cer albastru care, ne va duce tot mai sus"
       
        End If
        If (RadioButton4.Checked) Then
            My.Computer.Audio.Play(My.Resources.michael_jackson_billy_jean_lyrics, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "She was more like a beauty queen from a movie scene
I said, Don't mind, but what do you mean, I am the one
Who will dance on the floor in the round ?
She said I am the one, who will dance on the floor in the round.

She told me her name was Billie Jean, as she caused a scene
Then every head turned with eyes that dreamed of being the one
Who will dance on the floor in the round"
       
        End If
        If (RadioButton11.Checked) Then
            My.Computer.Audio.Play(My.Resources.Michael_Jackson_Beat_it__Lyrics_, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "They told him, Don't you ever come around here.
Don't want to see your face. You better disappear.
The fire's in their eyes and their words are really clear
So beat it, just beat it

You better run, you better do what you can
Don't want to see no blood, don't be a macho man
You want to be tough, better do what you can
So beat it, but you want to be bad"
       
        End If
        If (RadioButton12.Checked) Then
            My.Computer.Audio.Play(My.Resources.Michael_Jackson_They_Dont_Care_About_Us__Lyrics_, AudioPlayMode.Background)
            TextBox2.Visible = True
            TextBox2.Clear()
            TextBox2.Text = "Skin head, dead head
Everybody gone bad
Situation, aggravation
Everybody allegation
In the suite, on the news
Everybody dog food
Bang bang, shot dead
Everybody's gone mad

All I want to say is that
They don't really care about us
All I want to say is that
They don't really care about us"

        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        My.Computer.Audio.Stop()
        TextBox2.Visible = False
        TextBox1.Visible = False
        GroupBox2.Visible = False
        GroupBox3.Visible = False
        GroupBox4.Visible = False
        RadioButton1.Checked = False
        RadioButton2.Checked = False
        RadioButton3.Checked = False
        RadioButton4.Checked = False
        RadioButton5.Checked = False
        RadioButton6.Checked = False
        RadioButton7.Checked = False
        RadioButton8.Checked = False
        RadioButton9.Checked = False
        RadioButton10.Checked = False
        RadioButton11.Checked = False
        RadioButton12.Checked = False

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        My.Computer.Audio.Stop()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        My.Computer.Audio.Stop()

    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        My.Computer.Audio.Stop()


    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Application.Exit()


    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub
    
End Class
